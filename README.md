# Deteccao-Fraudes-Cliques

Projeto desenvolvido em R, como metodologia de aprendizado para um curso da Formação Cientista de Dados da Data Science Academy.
Tendo como objetivo prever se um usuário fará o download de um aplicativo depois de clicar em um anúncio de aplicativo para
dispositivos móveis.

Fonte dos dados: https://www.kaggle.com/c/talkingdata-adtracking-fraud-detection/data